#ifndef _LIBRARY_H
#define _LIBRARY_H

#include"date.h"

#define USER_DB             "user.bd"
#define BOOKS_DB            "books.db"
#define BOOKCOPY_DB         "bookcopy.db"
#define ISSUERECORD_DB      "issuerecord.db"
#define PAYMENT_DB          "payment.db"

#define ROLE_OWNER          "owner"
#define ROLE_LIBRARIAN      "librarian"
#define ROLE_MEMBER         "member"

#define EMAIL_OWNER         "sarang@sunbeam"

#define STATUS_AVAI         "available"
#define STATUS_ISSUED       "issues"

#define PAY_TYPE_FEES       "fees"
#define PAY_TYPE_FINE       "fine"

#define FINE_PER_DAY            5                
#define BOOK_RETURN_DAYS        7 
#define MEMBERSHIP_MONTH_DAYS   30   

typedef struct user{
    int id;
    char name[30];
    char email[20];
    char phone[12];
    char password[10];
    char role[15];
}user_t;

typedef struct book{
     int id;
    char name[50];
    char author[50];
    char subject[30];
    double price;
    char isbn[15];
}book_t;

typedef struct bookcopy{
    int id;
    int bookid;
    int rack;
    char status[16];
}bookcopy_t;

typedef struct issuerecord{
    int id;
    int copyid;
    int memberid;
    date_t issue_date;
    date_t return_duedate;
    date_t return_date;
    double fine_amount;
}issuerecord_t;

typedef struct payment{
    int id;
    int member_id;
    double amount;
    char type[10];
    date_t tx_time;
    date_t next_Payment_duedate;
}payment_t;

//USER FUNCTIO
void user_accept(user_t *u);
void user_display(user_t *u);

//book function
void book_accept(book_t *b);
void book_display(book_t *b);

//bookcopy
void bookcopy_accept(bookcopy_t *bc);
void bookcopy_display(bookcopy_t *bc);

//issuerecord
void issuerecord_accept(issuerecord_t *i);
void issuerecord_display(issuerecord_t *i);

//payment
void payment_accept(payment_t *p);
void payment_display(payment_t *p);

//owner fun_accept
void owner_area(user_t *u);
void appoint_librarian();
void display_all_book_catlog();
void bookdisplay(book_t *b);


//librarian  function
void librarian_area(user_t *u);
void add_member();
void add_book();
void book_edit_by_id();
void bookcopy_add();
void bookcopy_check_detail_availability();
void bookcopy_issue();
void bookcopy_change_status(int bookcopy_id,char status[]);
void display_issued_bookcopy(int member_id);
void bookcopy_return();
void display_all_members();
void userdisplay(user_t *u);
void change_rack();
void take_payment();
void payment_history(int member_id);
int is_paid_member(int memberid);
void take_fine(int memberid,double fine_amount);
void display_issued_bookcopy_history(int memb_id);

//memb fun
void member_area(user_t *u);
void bookcopy_checkavail();


//commom fun
void sign_in();
void sign_up();
void change_password(char email[],char password[]);
void user_add(user_t *u);
int user_find_by_email(user_t *u, char email[]);
void book_find_by_name(char name[]);
int get_user_next_id();
int get_next_book_id();
int get_next_bookcopy_id();
int get_next_issuerecord_id();
int get_next_payment_id();
void edit_profile(char email[],char password[]);




#endif