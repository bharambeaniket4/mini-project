#include<stdio.h>
#include<stdlib.h>
#include"library.h"
#include<string.h>


void user_accept(user_t *u)
{
   /*  printf("userid         :   ");
    scanf("%d",u->id); */
    u->id = get_user_next_id();
    printf("name           :   ");
    scanf("%s",u->name);
    printf("email          :   ");
    scanf("%s",u->email);
    printf("phone          :   ");
    scanf("%s",u->phone);
    printf("password       :   ");
    scanf("%s",u->password);
    strcpy(u->role, ROLE_MEMBER) ;
  
}
void user_display(user_t *u)
{
    printf("\nid           :   %d",u->id);
    printf("\nname         :   %s",u->name);
    printf("\nemail        :   %s",u->email);
    printf("\nphone        :   %s",u->phone);
    printf("\npassword     :   %s\n",u->password);
}




//book function
void book_accept(book_t *b)
{
    /* printf("\nbookid         :   ");
    scanf("%d",b->id);  */
    b->id = get_next_book_id();
    printf("name           :   ");
    scanf("%s",b->name);
    printf("author         :   ");
    scanf("%s",b->author);
    printf("subject        :   ");
    scanf("%s",b->subject);
    printf("price          :   ");
    scanf("%lf",&b->price);
    printf("isbn           :   ");
    scanf("%s",b->isbn);
}
void book_display(book_t *b)
{
    printf("id           :   %d",b->id);
    printf("\nname         :   %s",b->name); 
    printf("\nauthor       :   %s",b->author);
    printf("\nsubject      :   %s",b->subject);
    printf("\nprice        :   %lf",b->price);
    printf("\nisbn         :   %s\n",b->isbn);
    printf("\n");   
}




void bookcopy_accept(bookcopy_t *bc)
{
/*     printf("\nbookcopy_id   :   ");
    scanf("%d",bc->id); */
    printf("bookid        :   ");
    scanf("%d",&bc->bookid);
    printf("rack no       :   ");
    scanf("%d",&bc->rack);
    strcpy(bc->status, STATUS_AVAI);   
}
void bookcopy_display(bookcopy_t *bc)
{
    printf("\ncopy_unique_id :   %d",bc->id);
    printf("\nbook_id        :   %d",bc->bookid);
    printf("\nrack no        :   %d",bc->rack);
    printf("\nStatus         :   %s",bc->status);
    printf("\n"); 
}




void issuerecord_accept(issuerecord_t *i)
{
   /*  printf("\nIssueRecord_id  :   ");
    scanf("%d",i->id); */
    printf("Copy_unique_id    :   ");
    scanf("%d",&i->copyid );
    printf("member_id        :   ");
    scanf("%d",&i->memberid);
    printf("issue_date      :   ");
    date_accept(&i->issue_date); 
	i->return_duedate = date_add(i->issue_date, BOOK_RETURN_DAYS);
	memset(&i->return_date, 0, sizeof(date_t));
	i->fine_amount = 0.0;
}
void issuerecord_display(issuerecord_t *i)
{
    printf("\nissue_id        :   %d",i->id);
    printf("\nCopyUnique_id   :   %d",i->copyid);
    printf("\nmember_id       :   %d",i->memberid);
    printf("\n Fine           : %.2lf",i->fine_amount);
    printf("\nissue ");
	date_print(&i->issue_date);
	printf("return due ");
	date_print(&i->return_duedate);
	printf("return ");
	date_print(&i->return_date);
    printf("\n");
}




void payment_accept(payment_t *p)
{
    /* printf("\nPayment_id           :   ");
    scanf("%d",p->id); */
    printf("member_id            :   ");
    scanf("%d",&p->member_id );
    printf("amount               :   ");
    scanf("%lf",&p->amount);
  /*   printf("type(fees/fine)      :   ");
    scanf("%s",p->type); */
    strcpy(p->type,PAY_TYPE_FEES);
    p->tx_time = date_current();
    //if(strcmp(p->type, PAY_TYPE_FEES) == 0)
		p->next_Payment_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
	//else
		//memset(&p->next_Payment_duedate, 0, sizeof(date_t));
}
void payment_display(payment_t *p)
{
    printf("\nid                   :   %d",p->id);
    printf("\nmember_id            :   %d",p->member_id);
    printf("\namount               :   %lf",p->amount);
	printf("\npayment ");
	date_print(&p->tx_time);
	printf("payment due");
	date_print(&p->next_Payment_duedate);
    printf("\n");
}



void user_add(user_t *u)
{
    //open file
    FILE *fp; 
    fp = fopen(USER_DB,"ab");
    if(fp == NULL)
    {
        perror("\n failedto open");
        return;
    }
    //write user data into the file
    fwrite( u,sizeof(user_t),1,fp);
    printf("\n User add into file");

    //close file
    fclose(fp);
}



int user_find_by_email(user_t *u, char email[])
{
    //open the file for reading data
    FILE *fp;
    int found =0;
    fp = fopen(USER_DB,"rb");
    if(fp == NULL)
    {
        perror("\n failed to open");
        return found;
    }
    //read all user one by one
    while(fread(u, sizeof(user_t), 1, fp) > 0) {
		// if user email is matching, found 1
		if(strcmp(u->email, email) == 0) {
			found = 1;
			break;
		}
	}
	// close file
	fclose(fp);
	// return
	return found;
}
void book_find_by_name(char name[])
{
    FILE *fp;
	int found = 0;
	book_t b;
	// open the file for reading the data
	fp = fopen(BOOKS_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}
	// read all books one by one
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		// if book name is matching partially, found 1
		if(strstr(b.name, name) != NULL) {
			found = 1;
			book_display(&b);
		}
	}
	// close file
	fclose(fp);
	if(!found)
		printf("No such book found.\n");
}



int get_user_next_id()
{
    //open the file
    //chaange the position the last record
    //is read is successful get th max id
    //close max id
    //return maz+1

    FILE *fp;
	int max = 0;
	int size = sizeof(user_t);
	user_t u;
	// open the file
	fp = fopen(USER_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
        max = u.id;
		// if read is successful, get max (its) id
	// close the file
	fclose(fp);
	// return max + 1
	return max+1;
}
int get_next_book_id()
{
    FILE *fp;
	int max = 0;
	int size = sizeof(book_t);
	book_t u;
	// open the file
	fp = fopen(BOOKS_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
        max = u.id;
		// if read is successful, get max (its) id
	// close the file
	fclose(fp);
	// return max + 1
	return max+1;
}
int get_next_bookcopy_id()
{
    FILE *fp;
	int max = 0;
	int size = sizeof(bookcopy_t);
	bookcopy_t bc;
	// open the file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&bc, size, 1, fp) > 0)
        max = bc.id;
		// if read is successful, get max (its) id
	// close the file
	fclose(fp);
	// return max + 1
	return max+1;
}
int get_next_issuerecord_id()
{
    FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t ir;
	// open the file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&ir, size, 1, fp) > 0)
        max = ir.id;
		// if read is successful, get max (its) id
	// close the file
	fclose(fp);
	// return max + 1
	return max+1;
}
int get_next_payment_id()
{
    FILE *fp;
	int max = 0;
	int size = sizeof(payment_t);
	payment_t p;
	// open the file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&p, size, 1, fp) > 0)
        max = p.id;
		// if read is successful, get max (its) id
	// close the file
	fclose(fp);
	// return max + 1
	return max+1;
} 



 void edit_profile(char email[],char password[])
  {
	int found = 0;
	FILE *fp;
	user_t user;
	//open user db file
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL)
    {
		perror("Cannot open user file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&user, sizeof(user_t),1,fp)>0)
    {
		if (strcmp(email,user.email) == 0 && strcmp(password,user.password) == 0)
        {
			found = 1;
			break;
		}
	}	
	if(found) {
		long size = sizeof(user_t);
		user_t nu;
		nu.id = user.id;

		user_display(&user);
	 
		printf("email: ");
		scanf("%s", nu.email);
		printf("phone: ");
		scanf("%s", nu.phone);

		strcpy(nu.name, user.name);
		strcpy(nu.password, user.password);
		strcpy(nu.role, user.role);

		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite user details into the file
		fwrite(&nu, size, 1, fp);
		user_display(&nu);
		printf("user updated.\n");

	} 
	// close user file
	fclose(fp);
}

void change_password(char email[],char password[])
{
	int found = 0;
	FILE *fp;
	user_t user;
	//open user db file
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL){
		perror("Cannot open user file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&user, sizeof(user_t),1,fp)>0){
		if (strcmp(email,user.email) == 0 && strcmp(password,user.password) == 0){
			found = 1;
			break;
		}
	}	
	if(found) {
		long size = sizeof(user_t);
		user_t nu;
		nu.id = user.id;

		user_display(&user);
	 
		//printf("email: ");
		//scanf("%s", nu.email);

		printf("New_password: ");
		scanf("%s",nu.password);

		strcpy(nu.email, user.email);
		strcpy(nu.name, user.name);
		strcpy(nu.phone, user.phone);
        strcpy(nu.role, user.role);

		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite user details into the file
		fwrite(&nu, size, 1, fp);
		user_display(&nu);
		printf("user password updated.\n");

	} 
	// close user file
	fclose(fp);
} 

