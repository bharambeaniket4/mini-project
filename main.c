#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"date.h"
#include"library.h"

void date_tester() 
{
date_t d1 = {1, 1, 2000}, d2 = {31, 12, 2000};
date_t d = {1, 1, 2000};
date_t r = date_add(d, 366);
date_print(&r);
int diff = date_cmp(d1, d2);
printf("date diff: %d\n", diff);
}

void tester()
{
user_t u;
book_t b;
user_accept(&u);
user_display(&u);
book_accept(&b);
book_display(&b);
} 

void copybook_tester()
{
    bookcopy_t bc;
    bookcopy_accept(&bc);
    bookcopy_display(&bc);
}

void isssuerecord_tester()
{
    issuerecord_t ir;

    issuerecord_accept(&ir);
    issuerecord_display(&ir);
}

void Payment_tester()
{
    payment_t p;
    payment_accept(&p);
    payment_display(&p);
}

void sign_up()
{
    //input user details from user
    user_t u;
    user_accept(&u);
    //write user details into user file
    user_add(&u);
}


void sign_in(){
	char email[30], password[10];
	user_t u;
	int invalid_user = 0;
	// input email and password from user.
	printf("email: ");
	scanf("%s", email);
	printf("password: ");
	scanf("%s", password);
	// find the user in the users file by email.
	if(user_find_by_email(&u, email) == 1) {
		// check input password is correct.
		if(strcmp(password, u.password) == 0) {
			// special case: check for owner login
			if(strcmp(email, EMAIL_OWNER) == 0)
				strcpy(u.role, ROLE_OWNER);
			// if correct, call user_area() based on its role.
			if(strcmp(u.role, ROLE_OWNER) == 0)
				owner_area(&u);
			else if(strcmp(u.role, ROLE_LIBRARIAN) == 0)
				librarian_area(&u);
			else if(strcmp(u.role, ROLE_MEMBER) == 0)
				member_area(&u);
			else
				invalid_user = 1;
		}
		else
			invalid_user = 1;
	}
	else
		invalid_user = 1;

	if(invalid_user)
		printf("Invalid email, password or role.\n");
}

int main(void)
{
    
    //date_tester();
    //tester();
    //copybook_tester();
    //issuerecord_tester();
    //Payment_tester();

    /* int id = get_user_next_id();
    printf("\n ID   :   %d",id); */

 
    int choice;
    do
    { 
        printf("\n0. Exit");
        printf("\n1. Sign In");
        printf("\n2. Sign up");
        printf("\nEnter choice  :");
        scanf("%d",&choice);
        switch (choice)
        {
            case 1:
                    sign_in();
                break;
            case 2:
                    sign_up();
                break;
        
        }
    }while(choice != 0);   
    
  return 0; 
} 