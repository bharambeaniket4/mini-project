#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"library.h"

void owner_area(user_t *u)
{
    int choice;
    int user_id;
    char email[30];
    char password[30];
    do{
        printf("\n ^^^ Welcome Owner ^^^ ");
        printf("\n0. Sign out");
        printf("\n1. Appoint librarian");
        printf("\n2. Edit Profile");
        printf("\n3. Change Password");
        printf("\n4. Fees/Fine Reports");
        printf("\n5. Book availiability");
        printf("\n6. Book Categories/Subject");
        printf("\nEnter choice  :");
        scanf("%d",&choice);
        switch(choice)
        {
            case 1:     //appoint Librarian
                    appoint_librarian();
                break; 
             case 2:
                    //edit_profile(email,password); 
                    //not recommended
                break;
            case 3:
                   /*  printf("\nEnter user email   :");
                    scanf("%s",email);
                    printf("\nEnter user password   :");
                    scanf("%s",password);
                    change_password(email,password); */
                    //not recommended
                break;
            case 4:
                    //Not taken at this stage
                break;
            case 5:
                    bookcopy_checkavail();
                break;
            case 6:
                    display_all_book_catlog();
                break;                                                            
        }
    }while(choice != 0);


}



void appoint_librarian()
{
    // i/o librarian details
    user_t u;
    user_accept(&u);
    strcpy(u.role,ROLE_LIBRARIAN);
    //change user role to librarian
    strcpy(u.role,ROLE_LIBRARIAN);
    // add librarian in user file
    user_add(&u);

}

void display_all_book_catlog()
{
    FILE *fp;
	int found = 0;
    int n=1;
	book_t b;
	// open the file for reading the data
	fp = fopen(BOOKS_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}
	// read all books one by one
	while(fread(&b, sizeof(book_t), 1, fp) > 0)
     {
		// if book name is matching partially, found 1
		if(b.id== n)
        {
			found = 1;
			bookdisplay(&b);
            n++;
		}
	}
	// close file
	fclose(fp);
	if(!found)
		printf("No Books_catlog found.\n");
}

void bookdisplay(book_t *b)
{
    printf("\nid        :   %d",b->id);
    printf("\nname      :   %s",b->name);
    printf("\nauthor    :   %s",b->author);
    printf("\nsubject   :   %s",b->subject);
    //printf("\nprice     :   %lf",b->price);
    //printf("\nisbn      :   %s",b->isbn);
    printf("\n");
}