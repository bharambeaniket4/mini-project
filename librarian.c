#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"library.h"


void librarian_area(user_t *u)
{
    int choice;
    char name[80];
    int member_id;
    char email[30];
    char password[30];
    int memb_id;
 
    do{
        printf("\n ### Welcome Librarian ###");
        printf("\n0.  Sign out");
        printf("\n1.  Add Member");
        printf("\n2.  Edit Profile");
        printf("\n3.  Change Password");
        printf("\n4.  Add Book");
        printf("\n5.  Find Book");
        printf("\n6.  Edit Book ");
        printf("\n7.  Check Availiability ");
        printf("\n8.  Add BookCopy ");
        printf("\n9.  Change Rack ");
        printf("\n10. Issue Copy ");
        printf("\n11. Return Copy");
        printf("\n12. Take Payment");
        printf("\n13. Payment History");
        printf("\n14. Display all the members");
        printf("\n15. Display Issued Record");
        printf("\nEnter choice  :");
        scanf("%d",&choice);
        switch(choice)
        {
            case 1:
                    add_member();
                break;
            case 2:         //edit user prfile/uopdate
                    printf("\nEnter user email   :");
                    scanf("%s",email);
                    printf("\nEnter user password   :");
                    scanf("%s",password);
                    edit_profile(email,password);
                break;
            case 3:         //change password
                    printf("\nEnter user email   :");
                    scanf("%s",email);
                    printf("\nEnter user password   :");
                    scanf("%s",password);
                    change_password(email,password);
                break;
            case 4:         //add new book
                    add_book();
                break;
            case 5:         //find book
                    printf("\nEnter book name   :");
                    scanf("%s",name);
                    book_find_by_name(name);
                break;
            case 6:     //edit book
                    book_edit_by_id();
                break;   
            case 7:         //check book availiablity
                     bookcopy_check_detail_availability();
                break;
            case 8:         //add bookcopy
                    bookcopy_add();
                break;
            case 9:         //change rack
                     change_rack();
                break;
            case 10:        //issue copy
                     bookcopy_issue();
                break;
            case 11:        //return copy
                     bookcopy_return();
                break;
            case 12:        //take payment
                     take_payment(); 
                break;    
            case 13:        //payment history
                     printf("\nEnter member ID   :");
                     scanf("%d",&member_id);
                     payment_history(member_id); 
                break;    
            case 14:        //display all members
                     display_all_members();
                break;     
            case 15:
                    printf("\nEnter the member_id   :");
                    scanf("%d",&memb_id);
                    display_issued_bookcopy_history(memb_id);
                break;                                                                            
        }
    }while(choice != 0);
}


void add_member()
{
    // i/o librarian details
    user_t u;
    user_accept(&u);
    user_add(&u);
}

void add_book()
{
  	FILE *fp;
	// input book details
	book_t b;
	book_accept(&b);
	b.id = get_next_book_id();
	// add book into the books file
	// open books file.
	fp = fopen(BOOKS_DB, "ab");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
	// append book to the file.
	fwrite(&b, sizeof(book_t), 1, fp);
	printf("book added in file.\n");
	// close books file.
	fclose(fp);
}
 
void book_edit_by_id()
{
    book_t b;
    int id;
    int found =0;       
    //input book id from user
    printf("\nEnter book ID     :");
    scanf("%d",&id);
    //oprn BOOKS_FILE
    FILE *fp = fopen(BOOKS_DB, "rb+");
    if( fp == NULL)
    {
        perror("\n Cannot open th file");
        exit(1);
    }
    //read book one by onr and check if ID is found
    while(fread(&b,sizeof(book_t),1,fp)>0)
    {
        if(id == b.id)
        {
            found=1;
            break;
        }
    }
    //if found
    if(found)
    {   
        //input new books details from user
        book_t nb;
        book_accept(&nb);
        nb.id = b.id;
        //take file position one record behund
        long size = sizeof(book_t);
        fseek(fp,-size,SEEK_CUR);
        //over write the book details
        fwrite(&nb,size,1,fp);
        printf("\n Book is updated");
    }
    else
    //if not found
        //show msg book noot found
            printf("\n Book not found!!!");
    //close the book file
    fclose(fp);
}

  


void bookcopy_add()
{
    //input bookcopy details
    //add bookcopy into the file
    //open book copies file
    //append book copy into the file
    //close book file
    
    bookcopy_t b;
    bookcopy_accept(&b);
    b.id=get_next_bookcopy_id();
    FILE *fp = fopen(BOOKCOPY_DB,"ab");
    if( fp == NULL)
    {
        perror("\n failed to open the file");
        exit(1);
    }
    fwrite(&b,sizeof(bookcopy_t),1,fp);
    printf("\n bookopy is added to file");
    fclose(fp);
}


void bookcopy_check_detail_availability()
{
    int book_id;
    int count =0;
    //input book id from librarian
    printf("\nEnter book_id :   ");
    scanf("%d",&book_id);
    //open bookcopy file
    FILE *fp = fopen(BOOKCOPY_DB,"rb");
    if (fp == NULL)
    {
        perror("\nfailed to ope the file ");
        exit(1);
    }
    //read the file one by one
    bookcopy_t bc;
    while(fread(&bc,sizeof(bookcopy_t),1,fp)>0)
    {
    //if id match and status avial print copy details
        if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAI)==0) 
        {
            bookcopy_display(&bc);
            count++;
        } 
        
    }
    //if not fiund print msg
    if(count == 0)
    {
        printf("\n no copies avialable");
    }
    //close th copy
    fclose(fp);
}




void bookcopy_issue()
{
    FILE *fp;
    issuerecord_t ir;
    //Accept the issue record detials from the user
    issuerecord_accept(&ir);
    //::if user id not piad give error
    if(!is_paid_member(ir.memberid))
    {
        printf("\nmember is not paid");
        return; 
    }                                                                                                                                              
    //generate and assign new issue id
    ir.id = get_next_issuerecord_id();
    //open issue record file
    fp = fopen(ISSUERECORD_DB,"ab");
    if(fp == NULL)
    {
        perror("\n Failed to open the file");
        exit(1);
    }
    //append the reocrd in the file
    fwrite(&ir,sizeof(issuerecord_t),1,fp);
    //close the file
    fclose(fp);
    //mark the copy as issued
    bookcopy_change_status(ir.id,STATUS_ISSUED);

}


void bookcopy_change_status(int bookcopy_id,char status[])
{

    //OPEN BOOKCOPY FILE
    FILE *fp= fopen(BOOKCOPY_DB,"rb+");
    if (fp == NULL)
    {
        perror("\n Failed to open the file");
        exit(1);
    }
    //read one by one
    bookcopy_t bc;
    while(fread(&bc,sizeof(bookcopy_t),1,fp)>0)
    {
        //if bookcopy id is matching
        if(bookcopy_id == bc.bookid);
        // modify itd status
        strcpy(bc.status,status);
        // go one record back
        long size = sizeof(bookcopy_t);
        fseek(fp,-size,SEEK_CUR);
        // over write it into the file
        fwrite(&bc,sizeof(bookcopy_t),1,fp);
        break;
    }        
    //close the file
    fclose(fp);
}


int is_paid_member(int memberid)
{
    payment_t p;
    int paid =0;
    date_t now = date_current();
    FILE *fp = fopen(PAYMENT_DB,"rb");
    if(fp == NULL)
    {
        perror("\nFailed to open the file");
        exit(1);
    }
    while(fread(&p,sizeof(payment_t),1,fp)>0)
    {
        if(p.member_id == memberid && /* strcmp(p.type,PAY_TYPE_FINE) */p.next_Payment_duedate.day != 0 && date_cmp(now,p.next_Payment_duedate)<0)
        {
            paid = 1;
            break;
        }
    }
    fclose(fp);
    return paid;
}



/* void display_issued_bookcopy(int member_id)
{
     FILE *fp;
    //open issuerecord file
    fp = fopen(ISSUERECORD_DB,"rb+");
    if(fp ==NULL)
    {
        perror("\n Failed to open the file");
        return;
    }
    //read record one by one
    issuerecord_t ir;
    while(fread(&ir,sizeof(issuerecord_t),1,fp)>0)
    {
        //if member id is marching and issue date is zero,ptrint it
        if(ir.memberid == member_id && ir.return_date.day ==0)
        {
            issuerecord_display(&ir);
        }
    }
    //close the file
    fclose(fp);
} */

void display_issued_bookcopy(int member_id) {
	FILE *fp;
	issuerecord_t rec;
	// open issue records file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}

	// read records one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// if member_id is matching and return date is 0, print it.
		if(rec.memberid == member_id && rec.return_date.day == 0)
			issuerecord_display(&rec);
	}
	// close the file
	fclose(fp);
}




void bookcopy_return()
{   
    FILE *fp;
    int member_id;
    int record_id;
    int found =0;
    //input member id
    printf("\nEnter member_id   :   ");
    scanf("%d",&member_id);
    //print all issued book(not returned)
    display_issued_bookcopy(member_id);
    //input bookcopy id to return
    printf("\nEnter Issue_record ID :   ");
    scanf("%d",&record_id);
    //open issuerecord file
    fp = fopen(ISSUERECORD_DB,"rb+");
    if(fp ==NULL)
    {
        perror("\n Faile to open the file");
        exit(1);
    }
    ////read record one by one
    issuerecord_t ir;
    while(fread(&ir,sizeof(issuerecord_t),1,fp)>0)
    {
        //FIND LAST ISSUErecord of the copy
        if(record_id == ir.id)
        {
            found =1;
         //init the return date
            ir.return_date = date_current();
         //check for the fine amount
            int diff_days;
            diff_days = date_cmp(ir.return_date,ir.return_duedate);
         //update the fine amount if nay
            if(diff_days >0)
            {
                ir.fine_amount = diff_days*FINE_PER_DAY;
                take_fine(ir.memberid,ir.fine_amount );
                printf("\n Fine amount Rs   :  %.2lf/- is applied",ir.fine_amount);
                break;
            }
        }
        
    }
    if(found)
    {
     //go one record back
        long size = sizeof(issuerecord_t);
        fseek(fp,-size,SEEK_CUR);
     //over write the issue record
        fwrite(&ir, sizeof(issuerecord_t),1,fp);
     //PRINT UPDATES ISSUE RECORD
        printf("\n Issue record upadted after return");
        issuerecord_display(&ir)   ;
     //update th copy status to avialble
        bookcopy_change_status(ir.copyid,STATUS_AVAI);
    }
    //close the file
    fclose(fp);
}



void display_all_members()
{
    FILE *fp;
	int found = 0;
    int n=1;
	user_t u;
	// open the file for reading the data
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}
	// read all books one by one
	while(fread(&u, sizeof(user_t), 1, fp) > 0) {
		// if book name is matching partially, found 1
		if(u.id== n) {
			found = 1;
			userdisplay(&u);
            n++;
		}
	}
	// close file
	fclose(fp);
	if(!found)
		printf("No such user found.\n");
}
void userdisplay(user_t *u)
{
    printf("\nid           :   %d",u->id);
    printf("\nname         :   %s",u->name);
    printf("\nemail        :   %s",u->email);
    printf("\nphone        :   %s",u->phone);
    printf("\npassword     :   %s",u->password);
    printf("\n");
}


void change_rack()
{
	int copy_id, found = 0;
	long int size = sizeof(bookcopy_t);
	bookcopy_t bookcopy;

	// open book copy file
	FILE *fp = fopen(BOOKCOPY_DB, "rb+");
	if (fp == NULL)
	{
		perror("failed to open bookcopy file");
		return;
	} 

	// accept copy id
	printf ("enter copy id : ");
	scanf("%d",&copy_id);

	// read book copy file 
	while (fread(&bookcopy, size, 1, fp)> 0)
	{
		//if copy id is found
		if(copy_id == bookcopy.id)
		{
			found = 1;
			printf("current rack : %d\n", bookcopy.rack);

			printf ("new rack : ");
			scanf("%d",&bookcopy.rack);

			bookcopy.id = copy_id;
			strcpy(bookcopy.status, STATUS_AVAI);
			break;
		}
	}
	// adjust  ptr position behind
	fseek(fp, -size , SEEK_CUR);
	// write into file
	fwrite(&bookcopy, size , 1,fp);
	fclose(fp);
	if (found == 0)
		printf("\ncopy id not found");
	else 
		printf("\ncopy updated in the new rack");
}


 void take_payment()
{
    payment_t p;
    FILE *fp;
    strcpy(p.type,PAY_TYPE_FEES);
    p.id=get_next_payment_id();
    //accept fes payment
    payment_accept(&p);
    //open the file     
    fp=fopen(PAYMENT_DB,"ab");
    if(fp==NULL) {
		perror("cannot open poayment record file");
		return;
	}
    //appendthe payment data at the end
   fwrite(&p,sizeof(payment_t),1,fp);
   //cliose the file
   fclose(fp);
}

void take_fine(int memberid,double fine_amount)
{
    payment_t p;
    FILE *fp;
    strcpy(p.type,PAY_TYPE_FEES);
    p.id=get_next_payment_id();
    //accept fes payment
   p.member_id = memberid;
   p.amount = fine_amount;
   strcpy(p.type,PAY_TYPE_FINE);
   p.tx_time = date_current();
   memset(&p.next_Payment_duedate,0,sizeof(date_t));
    //open the file     
    fp=fopen(PAYMENT_DB,"ab");
    if(fp==NULL) {
		perror("cannot open poayment record file");
		return;
	}
    //appendthe payment data at the end
   fwrite(&p,sizeof(payment_t),1,fp);
   //cliose the file
   fclose(fp);
}
  
 

void payment_history(int member_id)
{
    //open file
    payment_t p;
    FILE *fp = fopen(PAYMENT_DB,"rb");
    if(fp == NULL)
    {
        perror("\n File opening Failed");
        exit(1);
    }
    //read payment one by one till EOF
    while(fread(&p,sizeof(payment_t),1,fp)>0)
        if(p.member_id == member_id) 
            payment_display(&p);
    //close file
    fclose(fp);
} 



void display_issued_bookcopy_history(int memb_id)
{
	FILE *fp;
	issuerecord_t rec;
	// open issue records file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}

	// read records one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// if member_id is matching and return date is 0, print it.
		if(rec.memberid == memb_id && rec.return_date.day == 0)
			issuerecord_display(&rec);
	}
	// close the file
	fclose(fp);
}