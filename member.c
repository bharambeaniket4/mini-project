#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"library.h"

void member_area(user_t *u)
{
    char name[80];
    int choice;
    int user_id;
    char email[30];
    char password[30];
    do{
        printf("\n *** Welcome Member *** ");
        printf("\n0. Sign out");
        printf("\n1. Find Book");
        printf("\n2. Edit Profile");
        printf("\n3. Change Password");
        printf("\n4. Book availiability");
        printf("\n5. List issued Book");
        printf("\nEnter Choice  :");
        scanf("%d",&choice);
        switch(choice)
        {
            case 1:
                    printf("\nEnter book name   :");
                    scanf("%s",name);
                    book_find_by_name(name);
                break;
            case 2:
                    printf("\nEnter user email   :");
                    scanf("%s",email);
                    printf("\nEnter user password   :");
                    scanf("%s",password);
                    edit_profile(email,password);
                break;
            case 3:
                    printf("\nEnter user email   :");
                    scanf("%s",email);
                    printf("\nEnter user password   :");
                    scanf("%s",password);
                    change_password(email,password);
                break;
            case 4:
                    bookcopy_checkavail();
                break;
            case 5:
                    display_issued_bookcopy(u->id);
                break;
                                                                    
        }
    }while(choice != 0);
}


void bookcopy_checkavail()
{
    int book_id;
    int count =0;
    //input book id from librarian
    printf("\nEnter book_id :   ");
    scanf("%d",&book_id);
    //open bookcopy file
    FILE *fp = fopen(BOOKCOPY_DB,"rb");
    if (fp == NULL)
    {
        perror("\nfailed to open the file ");
        exit(1);
    }
    //read the file one by one
    bookcopy_t bc;
    while(fread(&bc,sizeof(bookcopy_t),1,fp)>0)
    {
    //if id match and status avial count copy details
        if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAI)==0) 
        {
            count++;
        } 
        
    }
    //print the no of count
        printf("\n no of copies avialable   :   %d",count);

    //close th copy file
    fclose(fp);
}